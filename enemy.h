#ifndef ENEMY_H
#define ENEMY_H

#include "character.h"

class Enemy : public Character
{
public:
    Enemy(const int& position = 0);
};

#endif // ENEMY_H
