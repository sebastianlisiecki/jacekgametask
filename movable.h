#ifndef MOVABLE_H
#define MOVABLE_H

enum class Direction
{
    Left,
    Right
};

class  Movable
{
public:
    Movable() = default;
    Movable(const int& point, const Direction& direction);

    // move into direction in which object is directed
    void move();

    // move into given direction
    void move(const Direction& direction);

    int getPosition() const;
    void setPosition(const int &value);

    Direction getDirection() const;
    void setDirection(const Direction &direction);

protected:
    int m_position = 0;
    Direction m_direction = Direction::Right;
};

#endif // MOVABLE_H
