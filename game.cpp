#include "game.h"

#include <iostream>
#include <conio.h>
#include <chrono>
#include <thread>

const char emptySpace = ' ';

Game::Game() :
    m_level(Level(m_levelSize, DrawableType::Empty)),
    m_player(2),
    m_enemy(m_levelSize - 3)
{
    m_level[m_player.getPosition()] = DrawableType::Player;
    m_level[m_enemy.getPosition()] = DrawableType::Enemy;
}

void Game::run()
{
    while (!m_end)
    {
        render();
        handleInput();
        update();
    }
}

void Game::render()
{
    if (m_end)
        return;

    system("cls");

    const std::string verticalSpace = "\n\n";
    const char floor = '_';


    std::cout << verticalSpace;

    for (int i = 0; i < m_levelSize; ++i)
    {
        switch (m_level[i])
        {
        case DrawableType::Player:
            std::cout << m_player.face();
            break;

        case DrawableType::Enemy:
            std::cout << m_enemy.face();
            break;

        case DrawableType::Empty:
            std::cout << emptySpace;
            break;

        case DrawableType::Bullet:
            std::cout << m_bullet.face();
            break;

        default:
            std::cout << "Wrong drawable type";
        }
    }

    std::cout << std::endl;
//    std::cout << horizontalSpace;
    for (int i = 0; i < m_levelSize; ++i)
    {
        std::cout << floor;
    }

    std::cout << std::endl << std::endl;
    std::cout << "Player's health: " << m_player.getHealth() << std::endl;
    std::cout << "Enemy's health: " << m_enemy.getHealth() << std::endl;

    std::cout << std::endl << std::endl;
}

void Game::handleInput()
{
    if (m_end)
        return;

    if (m_bullet.inUse)
        return;

    short input = getch();

    if (input == Key::a || input == Key::A)
    {
        if (m_player.getPosition() <= 0)
            return;

        m_level[m_player.getPosition()] = DrawableType::Empty;
        m_player.move(Direction::Left);
        m_level[m_player.getPosition()] = DrawableType::Player;
    }
    else if (input == Key::d || input == Key::D)
    {
        if (m_level[m_player.getPosition() + 1] != DrawableType::Empty)
            return;

        m_level[m_player.getPosition()] = DrawableType::Empty;
        m_player.move(Direction::Right);
        m_level[m_player.getPosition()] = DrawableType::Player;
    }
    else if (input == Key::Num4)
    {
        if (m_level[m_enemy.getPosition() - 1] != DrawableType::Empty)
            return;

        m_level[m_enemy.getPosition()] = DrawableType::Empty;
        m_enemy.move(Direction::Left);
        m_level[m_enemy.getPosition()] = DrawableType::Enemy;
    }
    else if (input == Key::Num6)
    {
        if (m_enemy.getPosition() >= m_levelSize - 1)
            return;

        if (m_level[m_enemy.getPosition()] != DrawableType::Empty)

        m_level[m_enemy.getPosition()] = DrawableType::Empty;
        m_enemy.move(Direction::Right);
        m_level[m_enemy.getPosition()] = DrawableType::Enemy;
    }
    else if (input == Key::Enter)
    {
        m_bullet = m_player.shoot();
    }
    else if (input == Key::Num5)
    {
        m_bullet = m_enemy.shoot();
    }

}

void Game::update()
{
    if (m_bullet.inUse)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        m_bullet.move();

        switch (m_bullet.getDirection())
        {
        case Direction::Right:
            if (m_level[m_bullet.getPosition() - 1] == DrawableType::Bullet)
                m_level[m_bullet.getPosition() - 1] = DrawableType::Empty;
            break;

        case Direction::Left:
            if (m_level[m_bullet.getPosition() + 1] == DrawableType::Bullet)
                m_level[m_bullet.getPosition() + 1] = DrawableType::Empty;
            break;

        default:
            break;
        }

        m_level[m_bullet.getPosition()] = DrawableType::Bullet;
        checkBulletCollision();
    }
}

void Game::checkBulletCollision()
{
    if (m_bullet.getPosition() == m_player.getPosition())
    {
        m_player.getDamaged(m_bullet.damage());
        m_level[m_player.getPosition()] = DrawableType::Player;
        m_bullet.inUse = false;

        if (m_player.isDead())
        {
            system("cls");
            std::cout << "You lose!";
            m_end = true;
        }
    }
    else if (m_bullet.getPosition() == m_enemy.getPosition())
    {
        m_enemy.getDamaged(m_bullet.damage());
        m_level[m_enemy.getPosition()] = DrawableType::Enemy;
        m_bullet.inUse = false;

        if (m_enemy.isDead())
        {
            system("cls");
            std::cout << "You win!";
            m_end = true;
        }
    }
}

