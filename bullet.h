#ifndef BULLET_H
#define BULLET_H

#include "movable.h"
#include "drawable.h"

class Bullet : public Movable, public Drawable
{
public:
    Bullet() = default;
    Bullet(const int& point, const Direction& direction);

    int damage() const;

    bool inUse = false;
private:
    int m_damage = 1;

};

#endif // BULLET_H
