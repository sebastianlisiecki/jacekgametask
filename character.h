#ifndef CHARACTER_H
#define CHARACTER_H

#include "movable.h"
#include "drawable.h"
#include "bullet.h"

class Character : public Movable, public Drawable
{
public:
    Character() = default;
    Character(const char& face, const int &position, const Direction &direction);

    Bullet shoot();
    void getDamaged(const int& damage);
    bool isDead() const;

    int getHealth() const;

protected:
    int m_health = 3;
};

#endif // CHARACTER_H
