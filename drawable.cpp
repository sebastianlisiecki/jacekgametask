#include "drawable.h"

Drawable::Drawable(const char &face) :
    m_face(face)
{

}

char Drawable::face() const
{
    return m_face;
}

void Drawable::setFace(char face)
{
    m_face = face;
}
