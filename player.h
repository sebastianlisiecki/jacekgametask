#ifndef PLAYER_H
#define PLAYER_H

#include "character.h"

class Player : public Character
{
public:
    Player() = default;
    Player(const int& position);
};

#endif // PLAYER_H
