#include "movable.h"

Movable::Movable(const int &point, const Direction& direction) :
    m_position(point),
    m_direction(direction)
{

}

void Movable::move()
{
    m_position += (m_direction == Direction::Left) ? -1 : 1;
}

void Movable::move(const Direction &direction)
{
    m_position += (direction == Direction::Left) ? -1 : 1;
}

int Movable::getPosition() const
{
    return m_position;
}

void Movable::setPosition(const int &value)
{
    m_position = value;
}

Direction Movable::getDirection() const
{
    return m_direction;
}

void Movable::setDirection(const Direction &direction)
{
    m_direction = direction;
}
