#ifndef GAME_H
#define GAME_H

#include <vector>

#include "player.h"
#include "enemy.h"
#include "bullet.h"


class Game
{
public:
    Game();

    void run();

private:
    enum class DrawableType
    {
        Empty,
        Player,
        Enemy,
        Bullet
    };

    typedef std::vector<DrawableType> Level;

    enum Key
    {
        Esc = 27,
        A = 65,
        a = 97,
        D = 68,
        d = 100,
        Enter = 13,
        Num4 = 52,
        Num5 = 53,
        Num6 = 54
    };

    void render();
    void handleInput();
    void update();
    void checkBulletCollision();

    const int m_levelSize = 30;
    const int m_levelsCount = 3;

    Level m_level;
    int m_currentLevel = 1;

    Player m_player;
    Enemy m_enemy;
    Bullet m_bullet;

    bool m_end = false;
};


#endif // GAME_H
