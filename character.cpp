#include "character.h"

Character::Character(const char &face, const int& position, const Direction& direction) :
    Movable(position, direction),
    Drawable(face)
{

}

Bullet Character::shoot()
{
//    int position = (m_direction == Direction::Left) ? m_position - 1 : m_position + 1;

    Bullet bullet(m_position, m_direction);
    bullet.inUse = true;

    return bullet;
}

void Character::getDamaged(const int &damage)
{
    m_health -= damage;
}

bool Character::isDead() const
{
    return (m_health <= 0);
}

int Character::getHealth() const
{
    return m_health;
}
