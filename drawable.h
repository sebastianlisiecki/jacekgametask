#ifndef DRAWABLE_H
#define DRAWABLE_H


class Drawable
{
public:
    Drawable() = default;
    Drawable(const char& face);

    char face() const;
    void setFace(char face);

protected:
    char m_face = ' ';
};

#endif // DRAWABLE_H
