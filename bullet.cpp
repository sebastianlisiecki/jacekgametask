#include "bullet.h"

const char FACE = '-';

Bullet::Bullet(const int &point, const Direction &direction) :
    Movable(point, direction),
    Drawable(FACE)
{

}

int Bullet::damage() const
{
    return m_damage;
}

